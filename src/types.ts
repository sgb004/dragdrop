export type onDragEventListener = (event: DragEvent) => void;
export type onMouseEventListener = (event: Event) => void;
export type onTouchEventListener = (event: Event) => void;
