/**
 * @name DragDropItem
 * @version 1.0.0
 * @author sgb004
 * @description Class to apply drag and drop in an element of a list of HTML elements.
 */

import { onDragEventListener } from './types';

export type DragDropItemEvents = {
	onDragStart: onDragEventListener;
	onDragEnd: onDragEventListener;
	onDragOver: onDragEventListener;
	onDragEnter: onDragEventListener;
	onDragLeave: onDragEventListener;
	onDrop: onDragEventListener;
};

export type ExchangePosition = (origin: string, destiny: string) => void;

class DragDropItem {
	#item: Element;
	#parent: Element;
	#events: DragDropItemEvents;
	exchangePosition: ExchangePosition;
	positionAttribute: string;

	constructor(
		item: Element,
		parent: string | Element,
		exchangePosition: ExchangePosition,
		positionAttribute = 'data-position'
	) {
		this.#item = item;

		if (typeof parent === 'string') {
			const parentClassName: string = parent;
			this.#parent = item.closest(parentClassName);
		} else {
			this.#parent = parent;
		}

		this.#events = {
			onDragStart: this.#onDragStart.bind(this),
			onDragEnd: this.#onDragEnd.bind(this),
			onDragOver: this.#onDragOver.bind(this),
			onDragEnter: this.#onDragEnter.bind(this),
			onDragLeave: this.#onDragLeave.bind(this),
			onDrop: this.#onDrop.bind(this),
		};

		this.exchangePosition = exchangePosition;
		this.positionAttribute = positionAttribute;

		this.#item.addEventListener('dragstart', this.#events.onDragStart);
		this.#item.addEventListener('dragend', this.#events.onDragEnd);
		this.#item.addEventListener('dragover', this.#events.onDragOver);
		this.#item.addEventListener('dragenter', this.#events.onDragEnter);
		this.#item.addEventListener('dragleave', this.#events.onDragLeave);
		this.#item.addEventListener('drop', this.#events.onDrop);
	}

	destroy() {
		this.#item.removeEventListener('dragstart', this.#events.onDragStart);
		this.#item.removeEventListener('dragend', this.#events.onDragEnd);
		this.#item.removeEventListener('dragover', this.#events.onDragOver);
		this.#item.removeEventListener('dragenter', this.#events.onDragEnter);
		this.#item.removeEventListener('dragleave', this.#events.onDragLeave);
		this.#item.removeEventListener('drop', this.#events.onDrop);
	}

	#onDragStart(event: DragEvent) {
		const position = this.#item.getAttribute(this.positionAttribute);

		this.#item.classList.add('drag-start');
		this.#parent.classList.add('drag-over');

		if (event.dataTransfer && position) {
			event.dataTransfer.effectAllowed = 'move';
			event.dataTransfer.setData('position', position);
		}
	}

	#onDragEnd() {
		this.#item.classList.remove('drag-start');
		this.#parent.classList.remove('drag-over');
	}

	#onDragOver(event: Event) {
		event.preventDefault();
		return false;
	}

	#onDragEnter() {
		this.#item.classList.add('drag-over');
	}

	#onDragLeave() {
		this.#item.classList.remove('drag-over');
	}

	#onDrop(event: DragEvent) {
		event.stopPropagation();

		if (event.target instanceof HTMLElement && event.dataTransfer) {
			const origin = event.dataTransfer.getData('position');
			const destiny = event.target.getAttribute(this.positionAttribute);

			event.target.classList.remove('drag-over');

			if (origin != null && destiny != null && origin !== destiny) {
				this.exchangePosition(origin, destiny);
			}
		}

		return false;
	}
}

export default DragDropItem;
