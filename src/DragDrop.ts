/**
 * @name DragDrop
 * @version 1.0.0
 * @author sgb004
 * @description Class to apply drag and drop in a list of HTML elements.
 */

import DragDropItem from './DragDropItem';
import { onDragEventListener, onMouseEventListener, onTouchEventListener } from './types';

export type DragDropEvents = {
	onDragStart: onDragEventListener;
	onDragEnd: onDragEventListener;
	onMouseOver: onMouseEventListener;
	onMouseLeave: onMouseEventListener;
	onTouchMove: onTouchEventListener;
	onDragOvers: onDragEventListener[];
};

class DragDrop {
	#container: Element;
	#events: DragDropEvents;
	#attributeKey: string;
	#changed = false;
	#parentClassName: string | null;
	#parentDragOver: Element | null;
	#itemDragOver: Element | null;
	#originalItemDrag: Element | null;
	#targetTouchClient: { x: number; y: number } | null = null;

	constructor(
		container: Element,
		itemClassName: string,
		attributeKey = 'data-position',
		parentClassName: string | null = null
	) {
		this.#container = container;

		this.#events = {
			onDragStart: this.#onDragStart.bind(this),
			onDragEnd: this.#onDragEnd.bind(this),
			onMouseOver: this.#onMouseOver.bind(this),
			onMouseLeave: this.#onMouseLeave.bind(this),
			onTouchMove: this.#onTouchMove.bind(this),
			onDragOvers: [],
		};

		this.#attributeKey = attributeKey;

		this.#parentClassName = parentClassName;

		this.#container.classList.add('drag-drop');

		this.#addDragDropItems(itemClassName);

		if (this.#parentClassName == null) {
			this.#addListenersItemsParent(this.#container);
		} else {
			this.#addListenersItemsParents();
		}
	}

	destroy() {
		if (this.#parentClassName == null) {
			this.#removeListenersItemsParent(this.#container, 0);
		} else {
			this.#removeListenersItemsParents();
		}
	}

	get container() {
		return this.#container;
	}

	#addDragDropItems(itemClassName: string) {
		const items = this.container.querySelectorAll(itemClassName);

		for (let i = 0; i < items.length; i++) {
			items[i].setAttribute('draggable', 'true');
			items[i].setAttribute(this.#attributeKey, `${i}`);

			new DragDropItem(
				items[i],
				this.container,
				this.exchangePosition.bind(this),
				this.#attributeKey
			);
		}
	}

	exchangePosition(origin: string, destiny: string) {
		const originElement = this.#container.querySelector(`*[${this.#attributeKey}="${origin}"]`);
		const destinyElement = this.#container.querySelector(
			`*[${this.#attributeKey}="${destiny}"]`
		);

		if (originElement && this.#originalItemDrag && originElement === this.#originalItemDrag) {
			if (originElement.nextElementSibling === destinyElement) {
				originElement.insertAdjacentElement('beforebegin', destinyElement);
			} else if (originElement.previousElementSibling === destinyElement) {
				originElement.insertAdjacentElement('afterend', destinyElement);
			} else {
				const originElementSibling = this.#addTempSibling(originElement, 'origin');
				const destinyElementSibling = this.#addTempSibling(destinyElement, 'destiny');

				originElementSibling.insertAdjacentElement('afterend', destinyElement);
				destinyElementSibling.insertAdjacentElement('afterend', originElement);

				originElementSibling.parentNode.removeChild(originElementSibling);
				destinyElementSibling.parentNode.removeChild(destinyElementSibling);
			}
		}

		this.#changed = true;
	}

	#addTempSibling(element: Element, type: 'origin' | 'destiny') {
		const sibling = document.createElement('span');
		sibling.classList.add('drag-drop-sibling', `drag-drop-${type}-sibling`);
		sibling.style.display = 'none';
		element.insertAdjacentElement('afterend', sibling);
		return sibling;
	}

	#addListenersItemsParent(parent: Element) {
		const onDragOver = (event: DragEvent) => {
			event.preventDefault();
			this.#parentDragOver = parent;
		};

		this.#events.onDragOvers.push(onDragOver);

		parent.addEventListener('dragstart', this.#events.onDragStart);
		parent.addEventListener('dragend', this.#events.onDragEnd);
		parent.addEventListener('mouseover', this.#events.onMouseOver);
		parent.addEventListener('mouseleave', this.#events.onMouseLeave);
		parent.addEventListener('parentOver', this.#events.onMouseOver);
		parent.addEventListener('touchmove', this.#events.onTouchMove, { passive: true });
		parent.addEventListener('dragover', onDragOver);
	}

	#removeListenersItemsParent(parent: Element, id: number) {
		parent.removeEventListener('dragstart', this.#events.onDragStart);
		parent.removeEventListener('dragend', this.#events.onDragEnd);
		parent.removeEventListener('mouseover', this.#events.onMouseOver);
		parent.removeEventListener('mouseleave', this.#events.onMouseLeave);
		parent.removeEventListener('parentOver', this.#events.onMouseOver);
		parent.removeEventListener('touchmove', this.#events.onTouchMove);
		parent.removeEventListener('dragover', this.#events.onDragOvers[id]);
	}

	#addListenersItemsParents() {
		const itemsParents = this.#container.querySelectorAll(this.#parentClassName);
		for (let i = 0; i < itemsParents.length; i++) {
			this.#addListenersItemsParent(itemsParents[i]);
		}
	}

	#removeListenersItemsParents() {
		const itemsParents = this.#container.querySelectorAll(this.#parentClassName);
		for (let i = 0; i < itemsParents.length; i++) {
			this.#removeListenersItemsParent(itemsParents[i], i);
		}
	}

	#dispatchParentOver() {
		if (this.#targetTouchClient) {
			const elementFromPoint = document.elementFromPoint(
				this.#targetTouchClient.x,
				this.#targetTouchClient.y
			);

			elementFromPoint.dispatchEvent(new Event('parentOver'));

			this.#targetTouchClient = null;
		}
	}

	#onDragStart(event: DragEvent) {
		if (event.target instanceof HTMLElement) {
			this.#originalItemDrag = event.target;
		}
	}

	#onDragEnd(event: DragEvent) {
		if (!this.#changed && this.#parentDragOver && event.target instanceof HTMLElement) {
			this.#itemDragOver = event.target;
		}

		this.#originalItemDrag = null;

		this.#dispatchParentOver();
	}

	#onMouseOver() {
		if (!this.#changed && this.#parentDragOver && this.#itemDragOver) {
			this.#parentDragOver.appendChild(this.#itemDragOver);
		}
		this.#changed = false;
		this.#parentDragOver = null;
		this.#itemDragOver = null;
	}

	#onMouseLeave(event: Event) {
		if (this.#parentDragOver === event.target) {
			this.#changed = false;
			this.#parentDragOver = null;
			this.#itemDragOver = null;
		}
	}

	#onTouchMove(event: TouchEvent) {
		this.#targetTouchClient = {
			x: event.targetTouches[0].clientX,
			y: event.targetTouches[0].clientY,
		};
	}
}

export default DragDrop;
