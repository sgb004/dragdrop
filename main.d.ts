declare module '@wudev/drag-drop' {
	export type onDragEventListener = (event: DragEvent) => void;
	export type onMouseEventListener = (event: Event) => void;
	export type onTouchEventListener = (event: Event) => void;

	export type DragDropEvents = {
		onDragStart: onDragEventListener;
		onDragEnd: onDragEventListener;
		onMouseOver: onMouseEventListener;
		onMouseLeave: onMouseEventListener;
		onTouchMove: onTouchEventListener;
		onDragOvers: onDragEventListener[];
	};

	export type DragDropItemEvents = {
		onDragStart: onDragEventListener;
		onDragEnd: onDragEventListener;
		onDragOver: onDragEventListener;
		onDragEnter: onDragEventListener;
		onDragLeave: onDragEventListener;
		onDrop: onDragEventListener;
	};

	export type ExchangePosition = (origin: string, destiny: string) => void;

	/**
	 * @name DragDrop
	 * @description Class to apply drag and drop in a list of HTML elements.
	 */
	export default class DragDrop {
		/**
		 * Constructor
		 * @param container - Container element where drag and drop will be applied
		 * @param itemClassName - CSS class name of the draggable elements
		 * @param attributeKey - Attribute to store elements position. Optional, default 'data-position'
		 * @param parentClassName - CSS class name of the parent element. Optional, if not defined container is used
		 */
		constructor(
			container: Element,
			itemClassName: string,
			attributeKey?: string,
			parentClassName?: string | null
		);

		/**
		 * Destroys instance releasing listeners
		 */
		destroy(): void;

		/**
		 * Container element
		 */
		container: Element;

		/**
		 * Exchanges position of two elements
		 * @param origin - Position of dragged element
		 * @param destiny - Position of element to drop on
		 */
		exchangePosition(origin: string, destiny: string): void;
	}

	/**
	 * @name DragDropItem
	 * @description Class to apply drag and drop in an element of a list of HTML elements.
	 */
	export class DragDropItem {
		exchangePosition: ExchangePosition;
		positionAttribute: string;

		/**
		 * Constructor
		 * @param item - Element to apply drag and drop
		 * @param parent - Parent element
		 * @param exchangePosition - Function to exchange positions between list elements
		 * @param positionAttribute - Attribute to store element position. Optional, default 'data-position'
		 */
		constructor(
			item: Element,
			parent: string | Element,
			exchangePosition?: ExchangePosition,
			positionAttribute?: string
		);

		/**
		 * Destroys instance releasing listeners
		 */
		destroy(): void;
	}
}
