const { exec } = require('child_process');
const fs = require('fs');
const UglifyJS = require('uglify-js');

const cleanPath = (path) => path.replace(/\.\./g, '').replace(/\/\//g, '/');

const execCommand = (command) =>
	new Promise((resolve, reject) => {
		exec(command, (error, stdout, stderr) => {
			if (error) {
				reject(error);
				return;
			}

			resolve();
		});
	});

const getFileContent = (path) =>
	new Promise((resolve, reject) => {
		fs.readFile(`./${cleanPath(path)}`, 'utf8', (err, data) => {
			if (err) {
				reject(err);
				return;
			}

			resolve(data);
		});
	});

const saveFileContent = (fileName, content) =>
	new Promise((resolve, reject) => {
		fs.writeFile(`./${cleanPath(fileName)}`, content, (err) => {
			if (err) {
				reject(err);
				return;
			}

			resolve();
		});
	});

const getArg = (arg, def) => {
	const position = process.argv.indexOf(arg);
	let value = def;

	if (position > 1) {
		value = process.argv[position + 1];
	}

	return value;
};

const existArg = (arg) => process.argv.includes(arg);

const commentImportsExports = (data) => {
	let result = data.replaceAll('export ', '//export ');
	return result.replaceAll('import', '//import ');
};

const getConfig = () =>
	new Promise((resolve, reject) => {
		let configFile = getArg('--config', 'build-config.json');

		if (configFile) {
			configFile = cleanPath(configFile.trim());
			getFileContent(configFile)
				.then((data) => {
					const config = JSON.parse(data);
					if (!Array.isArray(config.files) || config.files.length == 0) {
						reject('No files selected.');
					} else if (typeof config.output !== 'string' || config.output === '') {
						reject('No output selected.');
					} else {
						resolve(config);
					}
				})
				.catch((error) => reject(error));
		} else {
			reject('No config file selected!');
		}
	});

const getContents = (contents) =>
	new Promise((resolve, reject) => {
		Promise.all(contents)
			.then((data) => {
				const minify = existArg('--no-minify') ? false : true;
				let finalFile = '';

				finalFile = data.map((content) => commentImportsExports(content));

				if (minify) {
					finalFile = UglifyJS.minify(finalFile);
					resolve(finalFile.code);
				} else {
					finalFile = finalFile.join('\n');
					resolve(finalFile);
				}
			})
			.catch((error) => reject(error));
	});

const showReady = () => {
	const twoDigits = (digit) => `${digit}`.padStart(2, '0');

	const date = new Date();
	const year = date.getFullYear();
	const month = twoDigits(date.getMonth() + 1);
	const day = twoDigits(date.getDate());
	const hour = twoDigits(date.getHours());
	const minutes = twoDigits(date.getMinutes());
	const seconds = twoDigits(date.getSeconds());

	console.log(`READY ${year}-${month}-${day} | ${hour}:${minutes}:${seconds}`);
};

const build = () => {
	let config;

	getConfig()
		.then((data) => {
			config = data;
			return execCommand('tsc');
		})
		.then(() => getContents(config.files.map((file) => getFileContent(file))))
		.then((content) => saveFileContent(config.output, content))
		.then(() => showReady())
		.catch((error) => console.error(error));
};

build();
