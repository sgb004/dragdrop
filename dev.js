const chokidar = require('chokidar');
const { exec } = require('child_process');

const watcher = chokidar.watch('./src');
const executeCommand = () =>
	exec('npm run build_no_minify', (err, stdout, stderr) => {
		if (err) {
			console.error(err);
		}
		console.log(stdout);
		console.error(stderr);
	});

watcher.on('change', executeCommand);

executeCommand();
